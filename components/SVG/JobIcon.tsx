import * as React from "react"

function SvgComponent(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 11 11"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M11 3.143H8.643V0H2.357v3.143H0V11h11V3.143zM3.929 1.57H7.07v1.572H3.93V1.57z"
        fill="#fff"
      />
    </svg>
  )
}

export default SvgComponent
