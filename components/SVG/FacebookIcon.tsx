import * as React from "react"

function SvgComponent(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 8 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M2.375 3.863v2.034H.833v2.489h1.542v7.398h3.167V8.386h2.122s.199-1.194.296-2.497H5.553V4.187c0-.255.344-.597.686-.597h1.725V1H5.618c-3.322 0-3.243 2.492-3.243 2.864z"
        fill="#31363D"
      />
    </svg>
  )
}

export default SvgComponent
