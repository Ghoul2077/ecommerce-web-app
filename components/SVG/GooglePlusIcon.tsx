import * as React from "react"

function SvgComponent(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 21 15"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M6.763 6.123v2.613s2.534-.004 3.566-.004c-.559 1.694-1.428 2.616-3.566 2.616-2.164 0-3.853-1.754-3.853-3.919 0-2.164 1.689-3.918 3.853-3.918 1.144 0 1.883.402 2.56.963.543-.543.498-.62 1.878-1.924a6.596 6.596 0 10-4.439 11.475c5.446 0 6.777-4.74 6.336-7.902H6.763zm11.886.13V3.969h-1.633v2.286h-2.35v1.633h2.35v2.35h1.633v-2.35h2.286V6.254h-2.286z"
        fill="#31363D"
      />
    </svg>
  )
}

export default SvgComponent
