import * as React from "react"

function SvgComponent(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 14 14"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M.263 5.568L3.47 6.692a.846.846 0 00.714-.072l7.124-4.296L5.432 7.62a.742.742 0 00-.249.547l-.02 4.589c-.002.412.594.533.771.156L7.503 9.57a.414.414 0 01.565-.178l4.16 2.19c.261.138.585-.032.599-.314L13.367.4c.013-.278-.283-.475-.555-.37L.252 4.859a.373.373 0 00.01.709z"
        fill="#34495E"
      />
    </svg>
  )
}

export default SvgComponent
