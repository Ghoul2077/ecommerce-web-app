import * as React from "react"

function SvgComponent(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      width="1em"
      height="1em"
      viewBox="0 0 17 17"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <g
        clipPath="url(#prefix__clip0)"
        fillRule="evenodd"
        clipRule="evenodd"
        fill="#31363D"
      >
        <path d="M9.307 8.351a1.532 1.532 0 00-3.065 0 1.532 1.532 0 003.065 0zM11.945.032H4.167A3.885 3.885 0 00.279 3.906v1.08H6a3.677 3.677 0 012.137-.682 3.68 3.68 0 012.137.682h5.56v-1.08A3.885 3.885 0 0011.945.032zm1.575 3.684c0 .238-.194.43-.432.43h-1.334a.432.432 0 01-.432-.43v-1.33c0-.236.195-.43.432-.43h1.334c.238 0 .432.194.432.43v1.33z" />
        <path d="M11.828 8.387a3.688 3.688 0 01-3.692 3.678 3.69 3.69 0 01-3.693-3.678c0-.559.127-1.088.351-1.564H.28v5.302a3.885 3.885 0 003.888 3.874h7.778a3.885 3.885 0 003.888-3.874V6.823h-4.356a3.63 3.63 0 01.35 1.564z" />
      </g>
      <defs>
        <clipPath id="prefix__clip0">
          <path fill="#fff" transform="translate(.28 .032)" d="M0 0h16v16H0z" />
        </clipPath>
      </defs>
    </svg>
  )
}

export default SvgComponent
