import Image from "next/image";
import Link from "next/link";
import React from "react";

const BANNER_DATA = {
  title: "The Style files",
  heading: "SMART WOMEN’S BRAND. SLOW FASHION, FAST, ALL",
  description:
    "Grumpy wizards make toxic brew for the evil Queen and Jack. One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin.",
  navLink: "/",
  image: "/images/banner1.png",
  alt: "Banner",
};

export default function ProductBanner() {
  return (
    <div className="grid md:grid-cols-2 grid-cols-1 bg-gray-200">
      <div className="relative min-h-screen/2 w-full">
        <Image
          layout="fill"
          objectFit="contain"
          className="pointer-events-none"
          src={BANNER_DATA.image}
          alt={BANNER_DATA.alt}
        />
      </div>
      <div className="md:pt-14 md:pb-52 py-10 px-4 flex flex-col">
        <p className="text-xs tracking-wider text-blue-600">
          {BANNER_DATA.title}
        </p>
        <p className="text-gray-800 max-w-xl text-3xl font-semibold my-5 uppercase">
          {BANNER_DATA.heading}
        </p>
        <p className="text-gray-800 max-w-md text-base my-2">
          {BANNER_DATA.description}
        </p>
        <Link href={BANNER_DATA.navLink}>
          <a className="underline text-blue-400 mt-4 focus:outline-none">
            Learn more
          </a>
        </Link>
      </div>
    </div>
  );
}
