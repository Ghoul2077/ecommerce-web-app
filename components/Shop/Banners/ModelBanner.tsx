import Image from "next/image";
import React from "react";

const BANNER_DATA = {
  title: "The Sweatshirt",
  caption: "Elevating an essential with touches of fine detailing.",
  description:
    "Our first lookbook embraces elegance in motion and features a selection of signature L’Estrange pieces that combine woven fabrics and custom-made knits.",
  navLink: "/",
  image: "/images/banner2.png",
  alt: "Banner",
};

export default function ModelBanner() {
  return (
    <div className="flex flex-col max-w-screen-xl mx-auto my-10 px-2 md:px-2">
      <div className="flex md:flex-row flex-col items-center py-10">
        <div className="relative h-96 w-full">
          <Image
            layout="fill"
            objectFit="cover"
            className="pointer-events-none"
            src={BANNER_DATA.image}
            alt={BANNER_DATA.alt}
          />
        </div>
        <div className="md:-ml-20 mt-2 mx-auto z-10">{BANNER_DATA.description}</div>
      </div>
      <div className="self-center text-right">
        <div className="flex items-center justify-end text-2xl font-semibold uppercase">
          <div className="border-t-4 border-gray-500 w-7 mr-2" />
          {BANNER_DATA.title}
        </div>
        <div className="text-gray-800">{BANNER_DATA.caption}</div>
      </div>
    </div>
  );
}
