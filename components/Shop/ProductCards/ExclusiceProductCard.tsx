import React from "react";
import Image from "next/image";
import Link from "next/link";
import clsx from "clsx";
import getSymbolFromCurrency from "currency-symbol-map";
import { calculateCardPrice } from "../../../utils/utils";

export default function ExclusiceProductCard({ data, align = "right" }) {
  return (
    <Link href="/products/[id]" as={`/products/${data.handle}`}>
      <button className="flex h-full w-full items-center bg-white rounded focus:outline-none overflow-hidden  hover:bg-gray-100">
        <div
          className={clsx("relative h-full w-1/3 overflow-hidden", {
            ["order-2"]: align === "right",
            ["order-none"]: align === "left",
          })}
        >
          <Image
            layout="fill"
            objectFit="cover"
            className="pointer-events-none"
            src={data.images[0].src}
            alt={data.images[0].alt}
          />
        </div>
        <div className="h-full flex flex-col w-2/3 text-left px-8 py-5">
          <p className="text-blue-500 uppercase font-medium">Exclusive</p>
          <p className="text-gray-800 text-3xl font-semibold my-5">
            {data.title}
          </p>
          <p className="text-gray-500 text-lg my-2">{data.description}</p>
          <div className="self-start border-2 border-gray-500 px-5 py-1 rounded-full my-5">
            $ {calculateCardPrice(data)}
          </div>
        </div>
      </button>
    </Link>
  );
}
