import React from "react";
import Image from "next/image";
import Link from "next/link";
import clsx from "clsx";
import { calculateCardPrice } from "../../../utils/utils";

export default function FullWidthProductCard({ data }) {
  return (
    <div className="flex sm:flex-row flex-col h-full w-full max-w-4xl mx-auto items-center rounded focus:outline-none overflow-hidden my-10">
      <div className="relative self-stretch min-h-screen-2/3 sm:w-1/3 overflow-hidden">
        <Image
          layout="fill"
          objectFit="cover"
          className="pointer-events-none"
          src={data.images[0].src}
          alt={data.images[0].alt}
        />
      </div>
      <div className="h-full flex flex-col justify-items-start sm:w-2/3 text-left px-8 sm:pt-36 sm:pb-52 py-5">
        <Link href="/products/[id]" as={`/products/${data.handle}`}>
          <a className="text-gray-800 text-4xl font-medium my-5 uppercase hover:underline">
            {data.title}
          </a>
        </Link>
        <p className="text-gray-800 max-w-md text-base my-2">
          {data.description}
        </p>
        <div className="self-start border-2 border-gray-500 px-5 py-1 rounded-full my-5">
          $ {calculateCardPrice(data)}
        </div>
      </div>
    </div>
  );
}
