import React from "react";
import Image from "next/image";
import Link from "next/link";
import getSymbolFromCurrency from "currency-symbol-map";
import { calculateCardPrice } from "../../../utils/utils";

export default function ProductCard({ data, className = "" }) {
  return (
    <Link href="/products/[id]" as={`/products/${data.handle}`}>
      <button
        className={`flex flex-col justify-center items-center bg-white rounded focus:outline-none overflow-hidden hover:bg-gray-100 sm:py-2 ${className}`}
      >
        <div className="relative lg:h-96 h-72 md:w-72 w-full overflow-hidden">
          <Image
            layout="fill"
            objectFit="cover"
            className="pointer-events-none"
            src={data.images[0].src}
            alt={data.images[0].alt}
          />
        </div>
        <div className="text-lg font-medium mt-2 text-gray-900">
          {data.title}
        </div>
        <div className="text-md font-medium my-1 text-gray-700">
          {`"${data.vendor}" ${data.productType}`}
        </div>
        <div className="text-md font-medium text-gray-500 my-1">
          $ {calculateCardPrice(data)}
        </div>
      </button>
    </Link>
  );
}
