import React from "react";

export default function Cart() {
  return (
    <div className="h-full flex flex-col justify-between">
      <div>
        <h2 className="text-2xl px-5 py-3 font-semibold">Shopping Cart</h2>
        <hr className="mt-3" />
      </div>
      <ul className="h-full w-full overflow-y-scroll scrollbar-thin"></ul>
      <div className="flex flex-col mb-2 px-3 text-left">
        <hr className="mb-3" />
        <div className="grid grid-cols-2 justify-items-center my-2">
          <div className="uppercase text-gray-800 w-24">Subtotal</div>
          <div className="w-20 text-right">$0</div>
        </div>
        <div className="grid grid-cols-2 justify-items-center my-2">
          <div className="uppercase text-gray-800 w-24">Taxes</div>
          <div className="w-20 text-right">$0</div>
        </div>
        <div className="grid grid-cols-2 justify-items-center my-2">
          <div className="uppercase text-gray-800 w-24">Total</div>
          <div className="w-20 text-right">$0</div>
        </div>
        <button className="w-full mt-3 mb-14 sm:my-2 py-3 px-3 uppercase font-semibold bg-gray-700 text-white rounded-md max-w-xs self-center">
          Checkout
        </button>
      </div>
    </div>
  );
}
