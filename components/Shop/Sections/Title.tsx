import React from "react";
import Link from "next/link";
import Job from "../../SVG/JobIcon";

const TITLE_DATA = {
  title: "The Style files",
  heading: "SMART WOMEN’S BRAND. SLOW FASHION, FAST, ALL",
  description:
    "Grumpy wizards make toxic brew for the evil Queen and Jack. One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin.",
  shopLink: "/products",
};

export default function Title() {
  return (
    <div className="max-w-4xl mx-auto flex flex-col justify-center items-center py-8 text-center">
      <div className="border-l-4 border-gray-600 h-10" />
      <p className="text-xs my-3 tracking-wider text-blue-600">
        {TITLE_DATA.title}
      </p>
      <h1 className="text-2xl sm:text-3xl font-bold text-gray-800">
        {TITLE_DATA.heading}
      </h1>
      <p className="my-5 max-w-lg px-5 sm:px-0 text-gray-600">
        {TITLE_DATA.description}
      </p>
      <Link href={TITLE_DATA.shopLink}>
        <button className="flex items-center justify-center bg-gray-700 text-white px-16 py-2 my-5 rounded-full">
          <Job className="mr-1" width={20} height={10} />
          <div className="text-lg">Shop Now</div>
        </button>
      </Link>
    </div>
  );
}
