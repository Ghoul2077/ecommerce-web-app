import React from "react";
import Image from "next/image";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import Link from "next/link";

const HERO_CONTENT = [
  {
    image: "/images/hero1.png",
    alt: "Girl wearing hip clothes",
    content: {
      title: "The style files",
      heading: "Mcq Alexander Mcqueen",
      description:
        "Grumpy wizards make toxic brew for the evil Queen and Jack. One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin.",
    },
    link: "/",
  },
  {
    image: "/images/hero2.png",
    alt: "Girl wearing hip clothes",
    content: {
      title: "The style files",
      heading: "Mcq Alexander Mcqueen",
      description:
        "Grumpy wizards make toxic brew for the evil Queen and Jack. One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin.",
    },
    link: "/",
  },
  {
    image: "/images/hero3.png",
    alt: "Girl wearing hip clothes",
    content: {
      title: "The style files",
      heading: "Mcq Alexander Mcqueen",
      description:
        "Grumpy wizards make toxic brew for the evil Queen and Jack. One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin.",
    },
    link: "/",
  },
];

export default function Hero() {
  return (
    <Carousel
      className="select-none"
      infiniteLoop
      showThumbs={false}
      showIndicators={false}
      showStatus={false}
      useKeyboardArrows
      emulateTouch
      swipeable
    >
      {HERO_CONTENT.map((item, id) => (
        <div className="relative w-screen max-h-screen" key={id}>
          <Image
            src={item.image}
            alt={item.alt}
            width={2500}
            height={1300}
            priority
          />
          <div className="legend text-lg opacity-100 bg-transparent lg:flex flex-col hidden max-w-xl left-1/4 top-1/4 transform translate-x-2/3 text-left tracking-wider">
            <h4>{item.content.title}</h4>
            <h3 className="text-6xl font-bold mb-4 max-w-lg tracking-widest">
              {item.content.heading}
            </h3>
            <h2 className="leading-8">{item.content.description}</h2>
            <Link href={item.link}>
              <button className="max-w-xs bg-gray-700 my-5 rounded-full py-2">
                Read More
              </button>
            </Link>
          </div>
        </div>
      ))}
    </Carousel>
  );
}
