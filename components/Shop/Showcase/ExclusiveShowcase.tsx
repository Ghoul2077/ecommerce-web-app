import React from "react";
import _ from "lodash";
import clsx from "clsx";
import ProductCard from "../ProductCards/ProductCard";
import ExclusiceProductCard from "../ProductCards/ExclusiceProductCard";

export default function ExclusiveShowcase({ data }) {
  return (
    <>
      {_.chunk(data, 3).map((productsArray, id) => (
        <div
          className="max-w-screen-xl w-full mx-auto grid md:grid-cols-4 grid-cols-2 md:gap-2 lg:gap-5 gap-y-5 md:my-5 my-5 px-4 xl:px-0"
          key={id}
        >
          <div className="grid grid-cols-2 gap-4 col-span-2">
            <ProductCard data={productsArray[0]} />
            <ProductCard data={productsArray[1]} />
          </div>
          <div className={clsx("col-span-2", { ["grid-row-1"]: id % 2 !== 0 })}>
            <ExclusiceProductCard
              data={productsArray[2]}
              align={id % 2 !== 0 ? "left" : "right"}
            />
          </div>
        </div>
      ))}
    </>
  );
}
