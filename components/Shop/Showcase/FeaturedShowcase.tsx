import React from "react";
import clsx from "clsx";
import ProductCard from "../ProductCards/ProductCard";
import { Carousel } from "react-responsive-carousel";
import useMediaQuery from "../../../hooks/useMediaQuery";

export default function FeaturedShowcase({ data }) {
  const small = useMediaQuery("(max-width: 640px)");

  return (
    <div className="relative bg-white xl:px-32 sm:px-10 px-4 py-5 my-10">
      <h2 className="text-center text-3xl font-semibold text-gray-700 my-10">
        Featured Products
      </h2>
      <div
        className={clsx(
          "hidden sm:grid sm:grid-cols-3 grid-cols-2 gap-5 justify-center items-start",
          {
            ["xl:grid-cols-4"]: data?.length >= 4,
          }
        )}
      >
        {data.map((product: any, id: number) => (
          <ProductCard data={product} key={id} />
        ))}
      </div>
      {data?.length >= 4 && (
        <button className="absolute right-10 top-1/2 flex items-center text-xl text-gray-600 focus:outline-none">
          <div className="border-t-4 border-gray-700 w-8 mr-2" />
          More
        </button>
      )}
      <Carousel
        className="bg-transparent sm:hidden select-none"
        infiniteLoop
        showThumbs={false}
        showIndicators={false}
        showStatus={false}
        centerMode={true}
        centerSlidePercentage={small ? 70 : 60}
        useKeyboardArrows
        emulateTouch
        swipeable
      >
        {data.map((product: any, id: number) => (
          <ProductCard
            className="w-52 md:w-auto mx-auto"
            data={product}
            key={id}
          />
        ))}
      </Carousel>
    </div>
  );
}
