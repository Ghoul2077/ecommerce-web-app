import React from "react";
import SearchIcon from "../SVG/SearchIcon";

export default function Search({ className = "" }) {
  return (
    <div className={`max-w-lg w-full mr-2 ${className}`}>
      <label htmlFor="search" className="sr-only">
        Search
      </label>
      <div className="relative">
        <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
          <SearchIcon />
        </div>
        <input
          id="nav-search"
          className="block w-full pl-10 pr-3 py-2 border-0 leading-5 bg-white placeholder-gray-500 focus:outline-none focus:placeholder-gray-400 sm:text-sm"
          placeholder="Search products"
        />
      </div>
    </div>
  );
}
