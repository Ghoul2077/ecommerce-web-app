import React, { useState } from "react";
import Image from "next/image";
import Link from "next/link";
import { Transition } from "@headlessui/react";
import Facebook from "../SVG/FacebookIcon";
import GooglePlus from "../SVG/GooglePlusIcon";
import Insta from "../SVG/InstaIcon";
import Twitter from "../SVG/TwitterIcon";
import Send from "../SVG/SendIcon";
import PlusIcon from "../SVG/PlusIcon";
import MinusIcon from "../SVG/MinusIcon";

const FOOTER_MENUS = [
  {
    heading: "Company",
    items: ["About Us", "Work Here", "Team", "Happenings", "Dealer Locator"],
  },
  {
    heading: "Service",
    items: ["Support", "FAQ", "Warranty", "Repair Center", "Contact Us"],
  },
  {
    heading: "Orders and Returns",
    items: ["Order", "Status", "Shipping", "Return Policy", "Payments"],
  },
];

export default function Footer() {
  const [toggleState, setToggleState] = useState(FOOTER_MENUS.map(() => false));

  function toggleItem(index: number) {
    setToggleState((states) => [
      ...states.slice(0, index),
      !states[index],
      ...states.slice(index + 1),
    ]);
  }

  return (
    <footer
      className="absolute bg-white border-b-8 border-gray-800 w-full top-full shadow-lg"
      aria-labelledby="footerHeading"
    >
      <h2 id="footerHeading" className="sr-only">
        Footer
      </h2>
      <div className="mx-auto max-w-6xl text-center lg:text-left px-5 lg:pr-8 xl:pr-0 grid lg:grid-cols-4 grid-cols-1 gap-3 justify-center items-center py-5 my-10 md:my-0">
        <p className="text-4xl px-10 font-semibold tracking-wide text-gray-800">
          Newsletter
        </p>
        <div>Get the day’s top news stories delivered to your inbox</div>
        <div className="w-full">
          <label htmlFor="newsletter-subscription" className="sr-only">
            Newsletter
          </label>
          <div className="relative">
            <input
              id="newsletter-subscription"
              className="block w-full pr-10 pl-3 py-3 border border-gray-300 rounded leading-5 bg-white placeholder-gray-500 focus:outline-none focus:placeholder-gray-400 focus:border-gray-400 focus:shadow-outline-blue sm:text-sm"
              placeholder="Enter your email here"
            />
            <button className="absolute inset-y-0 right-0 pr-3 flex items-center focus:outline-none">
              <Send width={15} height={15} />
            </button>
          </div>
        </div>
        <button className="mx-auto px-20 max-w-xs uppercase rounded-full font-semibold bg-gray-700 text-white py-2">
          Subscribe
        </button>
      </div>
      <hr />
      <div className="mx-auto max-w-7xl grid lg:grid-cols-7 grid-cols-1 gap-3 justify-center items-start md:py-12 py-4">
        <div className="relative lg:col-span-2 col-span-1 flex flex-col items-start justify-between text-xl px-5">
          <div className="uppercase">Payments Accepted</div>
          <div className="w-full flex flex-wrap my-5">
            <button className="focus:outline-none">
              <Image
                src="/images/Visa.svg"
                alt="Visa"
                width={100}
                height={50}
              />
            </button>
            <button className="focus:outline-none">
              <Image
                src="/images/Mastercard.svg"
                alt="Master Card"
                width={100}
                height={50}
              />
            </button>
            <button className="focus:outline-none">
              <Image
                src="/images/American Express.svg"
                alt="American Express"
                width={100}
                height={50}
              />
            </button>
            <button className="focus:outline-none">
              <Image
                src="/images/Maestro.svg"
                alt="Maestro"
                width={100}
                height={50}
              />
            </button>
            <button className="focus:outline-none">
              <Image
                src="/images/Diners Club.svg"
                alt="Diners Club"
                width={100}
                height={50}
              />
            </button>
          </div>
        </div>
        {FOOTER_MENUS.map((menu, index) => (
          <div
            className="col-span-1 flex flex-col xl:p-5 px-5 lg:p-0"
            key={index}
          >
            <h3 className="hidden lg:block md:text-left text-center text-sm font-bold text-gray-700 tracking-wider uppercase mb-2">
              {menu.heading}
            </h3>
            <ul className="hidden lg:block">
              {menu.items.map((item, id) => (
                <li className="my-3" key={id}>
                  <button className="focus:outline-none text-left w-full lg:w-auto">
                    {item}
                  </button>
                </li>
              ))}
            </ul>
            <button
              onClick={() => toggleItem(index)}
              className="flex lg:hidden justify-between text-sm font-bold text-gray-700 tracking-wider uppercase mb-2 py-3"
            >
              {menu.heading}
              {!toggleState[index] && <PlusIcon width={20} height={20} />}
              {toggleState[index] && <MinusIcon width={20} height={20} />}
            </button>
            <Transition
              show={toggleState[index]}
              enter="transition-opacity ease-linear duration-200"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="transition-opacity ease-linear duration-100"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
              className="shadow px-3 pb-5"
            >
              <ul>
                {menu.items.map((item, id) => (
                  <li className="my-3" key={id}>
                    <button className="focus:outline-none text-left w-full lg:w-auto">
                      {item}
                    </button>
                  </li>
                ))}
              </ul>
            </Transition>
          </div>
        ))}
        <div className="relative lg:col-span-2 col-span-1 flex flex-col lg:items-start items-center justify-between text-xl px-10">
          <div className="uppercase">Social</div>
          <div className="sm:max-w-xs w-full flex justify-between items-center my-5">
            <button className="focus:outline-none">
              <Facebook width={22} height={22} />
            </button>
            <button className="focus:outline-none">
              <GooglePlus width={30} height={30} />
            </button>
            <button className="focus:outline-none">
              <Insta width={21} height={21} />
            </button>
            <button className="focus:outline-none">
              <Twitter width={24} height={24} />
            </button>
          </div>
          <button className="focus:outline-none">
            <Image
              src="/images/appstore.svg"
              alt="Download from App Store"
              width={125}
              height={90}
            />
          </button>
          <div className="text-sm text-gray-500">
            <Link href="https://techfleekstudio.com">
              <a>@2021 Techfleek Studio</a>
            </Link>
          </div>
        </div>
      </div>
    </footer>
  );
}
