import React, { useEffect, useState } from "react";
import { Transition } from "@headlessui/react";
import { useRouter } from "next/dist/client/router";
import Link from "next/link";
import Image from "next/image";
import clsx from "clsx";
import Search from "./Search";
import Menu from "../SVG/MenuIcon";
import Cart from "../SVG/CartIcon";
import { useSidebar } from "../../contexts/SidebarProvider";

const NAV_MENU_ITEMS = [
  { label: "Men", url: "/collection/men" },
  { label: "Women", url: "/collection/women" },
  { label: "Kids", url: "/collection/kids" },
  { label: "Accessories", url: "/collection/accessories" },
];

export default function Navbar() {
  const router = useRouter();
  const [isOpen, setIsOpen] = useState(false);
  const [activeIndex, setActiveIndex] = useState(
    NAV_MENU_ITEMS.findIndex((item) => item.url === router.pathname)
  );
  const { toggleSidebar } = useSidebar();

  useEffect(() => {
    setActiveIndex(
      NAV_MENU_ITEMS.findIndex((item) => item.url === router.pathname)
    );
  }, [router.pathname]);

  const toggleMenu = () => setIsOpen((status) => !status);

  return (
    <nav className="bg-white fixed w-full shadow-sm z-40 top-0">
      <div className="flex items-center justify-between w-full py-3 px-2 xl:px-0 max-w-6xl mx-auto">
        <Link href="/">
          <a className="relative flex flex-nowrap items-center focus:outline-none mr-5">
            <Image
              src="/images/logo.png"
              alt="Logo"
              width={60}
              height={45}
              priority
            />
            <span className="hidden sm:block text-2xl text-gray-700 ml-1">
              TechFleek Studio
            </span>
          </a>
        </Link>
        <ul className="hidden h-full absolute left-1/2 transform -translate-x-1/2 xl:flex justify-between items-stretch">
          {NAV_MENU_ITEMS.map((item, index) => (
            <li
              key={index}
              className={clsx("mx-2 border-gray-600", {
                "border-b-2  font-semibold": activeIndex === index,
                "border-0": activeIndex !== index,
              })}
            >
              <Link href={item.url}>
                <a
                  className="w-full h-full inline-flex justify-center items-center focus:outline-none px-5"
                  onClick={() => setActiveIndex(index)}
                >
                  {item.label}
                </a>
              </Link>
            </li>
          ))}
        </ul>
        <div className="flex items-center">
          <Search className="hidden xl:block" />
          <button
            aria-label="Open Cart"
            className="focus:outline-none flex items-center"
            onClick={toggleSidebar}
          >
            <Cart width={27} height={27} />
          </button>
          <button
            aria-label="Open Menu"
            className={clsx(
              "xl:hidden ml-4 inline-flex items-center justify-center p-2 rounded-md hover:text-gray-100 hover:bg-gray-700 focus:outline-none",
              {
                ["bg-gray-700 text-white"]: isOpen,
                ["text-black"]: !isOpen,
              }
            )}
            onClick={toggleMenu}
          >
            <Menu width={27} height={27} />
          </button>
        </div>
      </div>
      <Transition
        show={isOpen}
        enter="transition-opacity ease-linear duration-200"
        enterFrom="opacity-0"
        enterTo="opacity-100"
        leave="transition-opacity ease-linear duration-300"
        leaveFrom="opacity-100"
        leaveTo="opacity-0"
        className="lg:hidden shadow-xl px-3 pb-5"
      >
        <hr className="mb-3" />
        <Search />
        <hr className="mt-3" />
        <div className="pt-2">
          {NAV_MENU_ITEMS.map((item, index) => (
            <Link href={item.url} key={index}>
              <a
                className={clsx(
                  "mt-1 block pl-3 pr-4 py-3 border-l-4 border-transparent text-base font-medium text-gray-600 hover:text-gray-800 hover:bg-gray-50 hover:border-gray-300 focus:outline-none",
                  {
                    ["text-gray-800 bg-gray-50 border-gray-300"]:
                      activeIndex === index,
                  }
                )}
                onClick={() => {
                  setActiveIndex(index);
                  setIsOpen(false);
                }}
              >
                {item.label}
              </a>
            </Link>
          ))}
        </div>
      </Transition>
    </nav>
  );
}
