import { NextComponentType, NextPageContext } from "next";
import { useRouter } from "next/dist/client/router";
import React, { memo, useEffect, useRef } from "react";

export default function useScrollPositionMemo(
  Component: NextComponentType<NextPageContext, any, {}>,
  pageProps: any
) {
  const router = useRouter();
  const retainedComponents = useRef({});

  // const isRetainableRoute = ROUTES_TO_RETAIN.includes(router.asPath);

  // Add Component to retainedComponents if we haven't got it already
  if (!retainedComponents.current[router.asPath]) {
    const MemoComponent = memo(Component);
    retainedComponents.current[router.asPath] = {
      component: <MemoComponent {...pageProps} />,
      scrollPos: 0,
    };
  }

  // Save the scroll position of current page before leaving
  const handleRouteChangeStart = (url) =>
    (retainedComponents.current[router.asPath].scrollPos = window.scrollY);

  // Save scroll position - requires an up-to-date router.asPath
  useEffect(() => {
    router.events.on("routeChangeStart", handleRouteChangeStart);
    return () => {
      router.events.off("routeChangeStart", handleRouteChangeStart);
    };
  }, [router.asPath]);

  // Scroll to the saved position when we load a retained component
  useEffect(() => {
    window.scrollTo(0, retainedComponents.current[router.asPath].scrollPos);
  }, [Component, pageProps]);
}
