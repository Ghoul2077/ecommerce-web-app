export const calculateCardPrice = (data: { variants: any[] }, type = "min") => {
  switch (type) {
    case "min":
      return data.variants.reduce(
        (min: number, curr: { price: number }) =>
          curr.price < min ? curr.price : min,
        Infinity
      );
    case "max":
      return data.variants.reduce(
        (max: number, curr: { price: number }) =>
          curr.price > max ? curr.price : max,
        0
      );
    case "range":
      return `${calculateCardPrice(data)} - ${calculateCardPrice(data, "max")}`;
  }
};
