import React from "react";
import Image from "next/image";
import { useRouter } from "next/dist/client/router";
import ProductCard from "../../components/Shop/ProductCards/ProductCard";
import Footer from "../../components/UI/Footer";
import { client } from "../../utils/shopify-client/ShopifyClient";

const COLLECTION_DETAILS: {
  [key: string]: { description: string; headerImage: string };
} = {
  men: {
    description:
      "Grumpy wizards make toxic brew for the evil Queen and Jack. One morning, when Gregor Samsa woke.",
    headerImage: "/images/mens_collection.png",
  },
  women: {
    description:
      "Grumpy wizards make toxic brew for the evil Queen and Jack. One morning, when Gregor Samsa woke.",
    headerImage: "/images/womens_collection.png",
  },
  kids: {
    description:
      "Grumpy wizards make toxic brew for the evil Queen and Jack. One morning, when Gregor Samsa woke.",
    headerImage: "/images/kids_collection.png",
  },
  accessories: {
    description:
      "Grumpy wizards make toxic brew for the evil Queen and Jack. One morning, when Gregor Samsa woke.",
    headerImage: "/images/acessories_collection.png",
  },
};

export default function index({ products }) {
  const router = useRouter();
  const { handle } = router.query;

  return (
    <div className="flex-1 w-full h-full">
      <div className="relative md:min-h-screen/2 min-h-screen/3 w-full">
        <Image
          key={Math.random()}
          src={COLLECTION_DETAILS[String(handle)].headerImage}
          alt={`${handle}'s banner`}
          layout="fill"
          objectFit="cover"
          objectPosition="center"
          priority
        />
        <div className="bg-black opacity-40 absolute w-full h-full top-0 left-0" />
        <div className="w-full sm:max-w-lg absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 text-center text-white">
          <p className="md:text-6xl sm:text-5xl text-4xl font-semibold my-5 capitalize">
            {handle !== "accessories" ? `${handle}'s clothes` : handle}
          </p>
          <p className="text-sm md:text-base my-2">
            {COLLECTION_DETAILS[String(handle)].description}
          </p>
        </div>
      </div>
      <div className="grid md:grid-cols-4 sm:grid-cols-3 grid-cols-2 mx-auto gap-x-12 gap-y-10 w-full p-5 max-w-screen-xl justify-start">
        {products.map((product: any, id: React.Key) => (
          <ProductCard key={id} data={product} className="rounded-lg" />
        ))}
      </div>
      <Footer />
    </div>
  );
}

export const getServerSideProps = async (context: any) => {
  let query = {
    query: `tag:'${context.params.handle}'`,
  };

  const products = await client.product.fetchQuery(query);

  return {
    props: {
      products: JSON.parse(JSON.stringify(products)),
    },
  };
};
