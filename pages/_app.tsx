import "../styles/globals.css";
import "../styles/carousel.css";
import React, { useEffect, useState } from "react";
import { Router } from "next/dist/client/router";
import Head from "next/head";
import { AppProps } from "next/app";
import { Toaster } from "react-hot-toast";
import Navbar from "../components/UI/Navbar";
import Loader from "../components/UI/Loader";
import { SidebarProvider } from "../contexts/SidebarProvider";
import { BackToTopProvider } from "../contexts/BackToTopProvider";
import useScrollPositionMemo from "../hooks/useScrollPositionMemo";
import useMediaQuery from "../hooks/useMediaQuery";

function MyApp({ Component, pageProps }: AppProps) {
  const [loading, setLoading] = useState(false);
  const small = useMediaQuery("(max-width: 640px)");
  useScrollPositionMemo(Component, pageProps);

  useEffect(() => {
    const start = () => {
      setLoading(true);
    };
    const end = () => {
      setLoading(false);
    };

    Router.events.on("routeChangeStart", start);
    Router.events.on("routeChangeComplete", end);
    Router.events.on("routeChangeError", end);

    return () => {
      Router.events.off("routeChangeStart", start);
      Router.events.off("routeChangeComplete", end);
      Router.events.off("routeChangeError", end);
    };
  }, []);

  return (
    <>
      <Head>
        <title>Ecommerce App</title>
        <link rel="Website Logo" href="/favicon.ico" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,400;0,700;0,900;1,300;1,400;1,700&display=swap"
          rel="stylesheet"
        />
        <meta name="description" content="Ecommerce app for new generation" />
        <meta name="keywords" content="Clothes, Fashion, Party" />
        <meta name="author" content="Prakhar Singhal" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <SidebarProvider>
        <BackToTopProvider>
          <div className="flex flex-col bg-gray-50 relative min-h-screen pt-16 font-lato">
            <Navbar />
            <Toaster
              position={small ? "top-center" : "top-right"}
              reverseOrder={false}
            />
            <Loader show={loading} />
            <Component {...pageProps} />
          </div>
        </BackToTopProvider>
      </SidebarProvider>
    </>
  );
}

export default MyApp;
