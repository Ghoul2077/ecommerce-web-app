import React, { useEffect, useRef, useState } from "react";
import Image from "next/image";
import clsx from "clsx";
import { client } from "../../utils/shopify-client/ShopifyClient";
import { Carousel } from "react-responsive-carousel";
import HeartIcon from "../../components/SVG/HeartIcon";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import "drift-zoom/src/css/drift-basic.css";

export default function ProductPage({ productDetails }) {
  const [selectedOptions, setSelectedOptions] = useState(
    productDetails.options.map((_product: any) => null)
  );
  const [selectedVariant, setSelectedVariant] = useState();
  const paneRef = useRef(null);

  useEffect(() => {
    // lazy loading, you also import is as regular
    let Drift: new (
      arg0: HTMLElement,
      arg1: {
        // paneContainer: paneRef.current,
        inlinePane: boolean;
        touchBoundingBox: boolean;
        handleTouch: boolean;
      }
    ) => any;
    if (typeof window !== "undefined") {
      Drift = require("drift-zoom").default;
    }
    const images = document.querySelectorAll(".carousel-image > div");
    images.forEach((node: HTMLElement) => {
      node.dataset.zoom = node.getElementsByTagName("img")[0].dataset.zoom;
      new Drift(node, {
        // paneContainer: paneRef.current,
        inlinePane: true,
        touchBoundingBox: true,
        handleTouch: true,
      });
    });
  }, []);

  useEffect(() => {
    const variantIndex = selectedOptions.includes(null)
      ? undefined
      : productDetails.options.reduce(
          (res: number, _curr: any, index: number, array: any[]) =>
            res +
            array
              .slice(index + 1)
              .reduce((res, curr) => res * curr.values.length, 1) *
              selectedOptions[index],
          0
        );
    setSelectedVariant(variantIndex);
  }, [selectedOptions]);

  function makeSelection(optionIndex: number, valueIndex: number) {
    setSelectedOptions((selection: number[]) => [
      ...selection.slice(0, optionIndex),
      valueIndex,
      ...selection.slice(optionIndex + 1),
    ]);
  }
  return (
    <div className="flex-1 grid grid-cols-5">
      <Carousel
        className="relative xl:flex flex-row-reverse xl:p-5 px-5 pt-5 rounded-full md:col-span-2 col-span-5 select-none"
        renderThumbs={(children) =>
          children.map((item: any, id: React.Key) => (
            <div className="relative w-full h-20" key={id}>
              {React.Children.map(item.props.children, (child) =>
                React.cloneElement(child, {
                  id: null,
                  ref: null,
                  "data-zoom": null,
                })
              )}
            </div>
          ))
        }
        infiniteLoop
        showIndicators={false}
        showStatus={false}
        useKeyboardArrows
        emulateTouch
        swipeable
      >
        {productDetails.images.map(
          (image: { src: string; altText: string }, id: React.Key) => (
            <div
              className="relative w-full xl:h-screen-without-nav h-screen-slider carousel-image"
              key={id}
            >
              <Image
                data-zoom={image.src}
                src={image.src}
                alt={image.altText || "Product Image"}
                layout="fill"
                objectFit="cover"
              />
            </div>
          )
        )}
      </Carousel>
      <div className="relative py-5 md:px-10 px-5 flex flex-col justify-between md:col-span-3 col-span-5 text-gray-800">
        <div
          id="drift"
          ref={paneRef}
          className="hidden md:block absolute max-w-md max-h-96 z-30 inset-0 top-5 rounded-xl pointer-events-none"
        />
        <div>
          <p className="text-xl text-gray-500 font-medium my-2">{`${productDetails.vendor} ${productDetails.productType}`}</p>
          <p className="md:text-5xl text-4xl font-semibold mt-1 mb-3 uppercase">
            {productDetails.title}
          </p>
          <p className="md:text-3xl text-2xl font-semibold my-5">
            $ {productDetails.variants[0].price}
          </p>
          <p className="text-gray-500 text-lg">{productDetails.description}</p>
          <button className="flex items-center justify-end font-semibold uppercase my-5 focus:outline-none text-blue-900">
            <div className="border-t-4 border-gray-500 w-12 mr-2 rounded" />
            <p>See Details</p>
          </button>
        </div>
        {productDetails.options.map(
          (option: { name: string; values: any[] }, optionIndex: number) => (
            <div className="my-1" key={optionIndex}>
              <h2 className="uppercase">{option.name}</h2>
              <div className="flex items-center max-w-lg flex-wrap my-2">
                {option.values.map(({ value }: any, valueIndex: number) => (
                  <button
                    aria-label={value}
                    onClick={() => makeSelection(optionIndex, valueIndex)}
                    key={valueIndex}
                    className={clsx(
                      "w-12 h-12 rounded-full border-2 focus:outline-none border-gray-500 text-sm mr-4 my-2",
                      {
                        ["bg-gray-700 text-white"]:
                          selectedOptions[optionIndex] === valueIndex &&
                          option.name.toLowerCase() !== "color",
                        ["border-4"]:
                          selectedOptions[optionIndex] === valueIndex &&
                          option.name.toLowerCase() === "color",
                      }
                    )}
                    style={{
                      background:
                        option.name.toLowerCase() !== "size" ? `${value}` : "",
                    }}
                  >
                    {option.name.toLowerCase() === "size" && value}
                  </button>
                ))}
                {option.name.toLowerCase() === "size" && (
                  <button className="ml-3">Size Guide</button>
                )}
              </div>
            </div>
          )
        )}
        <div className="flex flex-col items-start my-1">
          <button className="bg-gray-700 px-14 py-3 rounded-full text-white uppercase my-3 md:w-96 w-full mx-auto md:mx-0">
            Add to Bag
          </button>
          <button className="bg-transparent px-14 py-3 border-2 border-gray-500 rounded-full text-black uppercase my-3 md:w-96 w-full mx-auto md:mx-0">
            Shop The Look
          </button>
        </div>
        <button className="absolute right-5 top-8 flex items-center">
          <HeartIcon className="mr-1" width={20} height={20} />
          Add to Wishlist
        </button>
      </div>
    </div>
  );
}

export const getServerSideProps = async (context: any) => {
  const productDetails = await client.product.fetchByHandle(
    context.params.handle
  );

  return {
    props: {
      productDetails: JSON.parse(JSON.stringify(productDetails)),
    },
  };
};
