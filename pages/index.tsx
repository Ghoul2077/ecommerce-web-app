import React from "react";
import { client } from "../utils/shopify-client/ShopifyClient";
import Hero from "../components/Shop/Sections/Hero";
import Footer from "../components/UI/Footer";
import Title from "../components/Shop/Sections/Title";
import FeaturedShowcase from "../components/Shop/Showcase/FeaturedShowcase";
import ExclusiveShowcase from "../components/Shop/Showcase/ExclusiveShowcase";
import FullWidthProductCard from "../components/Shop/ProductCards/FullWidthProductCard";
import ProductBanner from "../components/Shop/Banners/ProductBanner";
import ModelBanner from "../components/Shop/Banners/ModelBanner";

const HomePage = (props: any) => {
  const { homepageCollection, featuredCollection, highlight } = props;

  return (
    <>
      <Hero />
      <Title />
      {homepageCollection && (
        <ExclusiveShowcase data={homepageCollection.products} />
      )}
      {featuredCollection && (
        <FeaturedShowcase data={featuredCollection.products} />
      )}
      {highlight && <FullWidthProductCard data={highlight.products[0]} />}
      <ProductBanner />
      <ModelBanner />
      <Footer />
    </>
  );
};

export const getServerSideProps = async (context: any) => {
  const homepageCollection = await client.collection.fetchByHandle("frontpage");
  const highlight = await client.collection.fetchByHandle("highlight");
  const featuredCollection = await client.collection.fetchByHandle(
    "featured-collection"
  );

  return {
    props: {
      homepageCollection: JSON.parse(JSON.stringify(homepageCollection)),
      featuredCollection: JSON.parse(JSON.stringify(featuredCollection)),
      highlight: JSON.parse(JSON.stringify(highlight)),
    },
  };
};

export default HomePage;
