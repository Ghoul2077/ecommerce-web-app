module.exports = {
  purge: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./contexts/*.{js,ts,jsx,tsx}",
  ],
  darkMode: false, // or 'media' or 'class'
  important: true,
  theme: {
    extend: {
      height: () => ({
        "screen/2": "50vh",
        "screen-2/3": "calc(100vh * 2/3)",
        "screen/3": "calc(100vh / 3)",
        "screen/4": "calc(100vh / 4)",
        "screen/5": "calc(100vh / 5)",
        "screen-9/10": "calc(90vh)",
      }),
      minHeight: {
        "screen/2": "calc(100vh / 2)",
        "screen/3": "calc(100vh / 3)",
        "screen-2/3": "calc(100vh * 2/3)",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require("tailwind-scrollbar")],
};
