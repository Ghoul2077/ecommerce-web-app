import React, { createContext, useContext, useState } from "react";
import { Transition } from "@headlessui/react";
import Cart from "../components/Shop/Cart/Cart";
import Close from "../components/SVG/CloseIcon";

export const SidebarContext = createContext(null);
export const useSidebar = () => useContext(SidebarContext);

export function SidebarProvider({ children }) {
  const [toggleState, setToggleState] = useState(false);

  function toggleSidebar() {
    setToggleState((toggleState) => !toggleState);
  }

  return (
    <SidebarContext.Provider value={{ toggleSidebar }}>
      <Transition
        show={toggleState}
        className="fixed z-50 flex overflow-hidden"
      >
        <Transition.Child
          enter="transition-opacity ease-linear duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="transition-opacity ease-linear duration-300"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div
            className="bg-black opacity-30 w-screen h-screen"
            onClick={toggleSidebar}
          />
        </Transition.Child>
        <Transition.Child
          enter="transition ease-in-out duration-300 transform"
          enterFrom="translate-x-full"
          enterTo="-translate-x-0"
          leave="transition ease-in-out duration-300 transform"
          leaveFrom="translate-x-0"
          leaveTo="translate-x-full"
          className="bg-white h-screen sm:w-96 w-full fixed right-0 shadow-md"
        >
          <button
            onClick={toggleSidebar}
            className="absolute right-5 top-5 focus:outline-none"
          >
            <Close width={25} height={25} />
          </button>
          <Cart />
        </Transition.Child>
      </Transition>
      {children}
    </SidebarContext.Provider>
  );
}
