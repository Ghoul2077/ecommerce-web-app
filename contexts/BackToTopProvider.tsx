import React, { createContext, useContext, useState } from "react";
import Scroll from "react-scroll";
import { Transition } from "@headlessui/react";
import { useScrollPosition } from "@n8tb1t/use-scroll-position";

export const BackToTopContext = createContext(null);
export const useBackToTop = () => useContext(BackToTopContext);

const STOP_SCROLL_AT = 300;

export function BackToTopProvider({ children }) {
  const [visible, setVisible] = useState(false);

  useScrollPosition(({ currPos }) => {
    if (currPos.y < -STOP_SCROLL_AT) {
      setVisible(true);
    } else {
      setVisible(false);
    }
  });

  function goToTop() {
    const scroll = Scroll.animateScroll;
    scroll.scrollToTop();
  }

  return (
    <BackToTopContext.Provider value={{}}>
      <Transition
        show={visible}
        appear={true}
        enter="transition-opacity ease-linear duration-300"
        enterFrom="opacity-0"
        enterTo="opacity-100"
        leave="transition-opacity ease-linear duration-300"
        leaveFrom="opacity-100"
        leaveTo="opacity-0"
      >
        <button
          onClick={goToTop}
          className="fixed z-30 right-2 bottom-3 md:right-5 md:bottom-5 w-28 h-12 shadow opacity-50 hover:opacity-100 transition-all rounded-full border border-gray-200 bg-gray-700 text-white"
        >
          Back to Top
        </button>
      </Transition>
      {children}
    </BackToTopContext.Provider>
  );
}
